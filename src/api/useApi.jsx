import request from "./../utils/request";

/**
 * 指定模块的增删查改api
 * @param {} url
 * @returns
 */
export default function useApi(url) {
  const baseUrl = "/api";

  return {
    /**
     * 获取列表
     * @param {} data
     * @returns
     */
    getList(data) {
      return request.post(`${baseUrl}${url}/page`, data);
    },

    /**
     * 删除
     * @param {} data
     * @returns
     */
    delData(ids) {
      return request.post(`${baseUrl}${url}/delete`, { ids });
    },

    /**
     * 新增
     * @param {} data
     * @returns
     */
    addData(data) {
      return request.post(`${baseUrl}${url}/add`, data);
    },

    /**
     * 编辑
     * @param {} data
     * @returns
     */
    updateData(data) {
      return request.post(`${baseUrl}${url}/update`, data);
    },

    /**
     * 获取详情
     * @param {} id
     * @returns
     */
    getDataInfo(id) {
      return request.get(`${baseUrl}${url}/id=${id}`);
    },
  };
}
