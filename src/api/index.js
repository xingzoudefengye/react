import request from "../utils/request";


export default {

  getCompany(data) {
    return request.post("/api/admin/storeManage/cateringCompany/page", data);
  },
  getArea() {
    return request.post("/api/admin/base/open/areas");
  },
  delCompany(ids) {
    return request.post("/api/admin/storeManage/cateringCompany/delete", {
      ids,
    });
  },
  addCompany(data) {
    return request.post("/api/admin/storeManage/cateringCompany/add", data);
  },
  getCompanyInfo(id) {
    return request.get("/api/admin/storeManage/cateringCompany/info?id=" + id);
  },
  updateCompany(data) {
    return request.post("/api/admin/storeManage/cateringCompany/update", data);
  },

};
