import request from "../utils/request";



export default {
  /**
   * 登录
   */
  login(data) {
    return request.post("/api/admin/base/open/login", data);
  },

  /**
   * 获取用户信息
   */
  getUserInfo() {
    return request.get("/api/admin/base/comm/person");
  },
};
