// 格式化日期
export const famterDate = (date) => {
  if (!date) {
    return "";
  }

  return `${new Date(date).getFullYear()}-${
    new Date(date).getMonth() + 1
  }-${new Date(date).getDate()}`;
};
