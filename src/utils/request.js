import axios from "axios";
import { message } from "antd";


axios.interceptors.request.use((config) => {
  config.headers.Authorization = localStorage.getItem("token");
  return config;
});


axios.interceptors.response.use(
  (response) => {
    if (response.data.code !== 1000) {
      message.error(response.data.message);
      throw new Error(response.data.message);
    }
    return response.data.data;
  },
  (error) => {
    message.error(error.response.data.message);
    if (error.response.status === 401) {
      location.href = `${location.origin}/#/login`;
    }
  }
);
export default axios;