import { configureStore } from "@reduxjs/toolkit";
import { userSlice } from "./slices/user";

// 初始化redux
const store = configureStore({
  reducer: {
    user: userSlice.reducer,
  },
});

/**
 * 监听每次数据的变化
 * 变化的时候把数据存到本地存储
 */
store.subscribe(() => {
  const { user } = store.getState();
  localStorage.setItem('user', JSON.stringify(user))
});

export default store;
