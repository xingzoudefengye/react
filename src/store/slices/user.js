import { createSlice } from "@reduxjs/toolkit";

// 获取本地存储数据
const getLocalState = () => {
  const data = localStorage.getItem("user");

  // 如果本地有缓存，那么优先取缓存
  if (data) {
    return JSON.parse(data);
  }

  // 没有则使用初始化数据
  return {
    roles: [],
    token: "",
  };
};

export const userSlice = createSlice({
  name: "user",
  initialState: getLocalState(),

  reducers: {
    setUserInfo(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const { setUserInfo } = userSlice.actions;
