import Company from "../company/company";
import CulturalAuditorium from "../culturalAuditorium/culturalAuditorium";
import Supervise from "../supervise/supervise";
import User from "../user/user";
import { useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";


//此方法里面调用接口替换数据
export const getMenus = () => {
  return [

    {
      key: "/storeManage",
      path: "/storeManage",
      label: "门店管理",
      roles: ["inspec", "familyPartyRecord"],
      children: [
        {
          key: "/storeManage/company",
          path: "/storeManage/company",
          label: "餐饮单位列表",
          roles: ["inspec"],
          element: <Company />,
        },
        {
          key: "/storeManage/culturalAuditorium",
          label: "家宴中心",
          roles: ["familyPartyRecord"],
          path: "/storeManage/culturalAuditorium",
          element: <CulturalAuditorium />,
        },
      ]
    },
    {
      key: "/inspec",
      label: "监督检查",
      roles: ["inspec"],
      path: "/inspec",
      children: [
        {
          label: "监督检查列表",
          key: "/inspec/supervise",
          roles: ["inspec"],
          path: "/inspec/supervise",
          element: <Supervise />,
        },
      ]
    },
    {
      label: "权限管理",
      key: "/sys",
      roles: ["admin"],
      path: "/sys",
      children: [
        {
          label: "用户列表",
          key: "/sys/user",
          roles: ["admin"],
          path: "/sys/user",
          element: <User />,
        },
      ]
    },
  ];
};



/**
 * 登录拦截hooks
 */
export const useListenRouter = (cb) => {
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    cb(location);
    // 每次路由变化判断当前是否登录
    if (!localStorage.getItem("token")) {
      // 如果没有跳转到登录页面
      navigate("/login");
    }
  }, [location.pathname]);
};
