import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  CloseCircleOutlined
} from '@ant-design/icons';
import { Layout, Menu, Avatar, Dropdown, Space ,Button} from 'antd';
import React, { useState } from 'react';
const { Header, Sider, Content } = Layout;
import "./home.scss"
import { useLocation, useNavigate, useRoutes } from "react-router-dom";

import { getMenus, useListenRouter } from './menu';

import { useSelector } from "react-redux";

export default function Home() {



  const [collapsed, setCollapsed] = useState(false);

  const items = [
    {
      label: '退出登录',
      key: '0',
    },

  ];

  const navigate = useNavigate()

  const location = useLocation();

  const [navs, setNavs] = useState([]);

  const user = useSelector(({ user }) => user);


  /**
    * 监听路由变化,添加面包屑数据
    */
  useListenRouter((data) => {

    if (navs.some((item) => item.pathname === data.pathname) || data.pathname === '/') {
      return;
    }
    setNavs([
      ...navs,
      {
        pathname: data.pathname,
        label: getMenuName(data.pathname),
      },
    ]);
  });


  const logout = ({ key }) => {
    if (key === "0") {
      localStorage.clear();
      navigate("/login");
    }
  };


  const linkPage = ({ key }) => {
    navigate(key);
  };

  /**
   * 根据当前角色获取当前有权限的菜单
   */
  const getCurrentMenus = (menus = getMenus()) => {
    let currentRoles = user.roles
    if (localStorage.getItem("roles")) {
      currentRoles = JSON.parse(localStorage.getItem("roles"));
    }

    return menus.filter((item) => {
      if (item.children) {
        item.children = getCurrentMenus(item.children);
      }
      return item.roles.some((role) => currentRoles.includes(role));
    });
  };



  const delNav = (event, index) => {
    // 阻止冒泡
    event.stopPropagation();

    /**
     * 拿到当前删除的那一项
     */
    const [delItem] = navs.splice(index, 1);

    // 当前删除的那一项，是高亮的
    if (delItem.pathname === location.pathname) {
      navigate(navs[navs.length - 1].pathname);
    }

    // 渲染
    setNavs([...navs]);
  };

  /**
   * 通过路由的path，拿到菜单的名字
   */
  const getMenuName = (path) => {
    let label = "";

    const getName = (menus = getCurrentMenus()) => {
      menus.forEach((item) => {
        if (item.path === path) {
          label = item.label;
          return;
        }

        if (item.children) {
          getName(item.children);
        }
      });
    };
    getName();

    return label;
  };



  const routerComps = useRoutes(getCurrentMenus());

  return (
    <Layout className='layout'>

      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="demo-logo-vertical" >管理 </div>
        <Menu
          onClick={linkPage}
          theme="dark"
          mode="inline"
          items={getCurrentMenus()}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{ padding: 0, }} >

          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed),
          })}

          <Space>
            {navs.map((item, index) => (
              <Button
                onClick={() => navigate(item.pathname)}
                type={location.pathname === item.pathname ? "primary" : "default"}
                key={index}
              >
                {item.label}
                {navs.length !== 1 && (
                  <CloseCircleOutlined
                    onClick={(event) => delNav(event, index)}
                  />
                )}
              </Button>
            ))}
          </Space>
          <Dropdown
            menu={{
              items,
              onClick: logout
            }}
            trigger={['click']}
          >

            <a onClick={(e) => e.preventDefault()}>
              <Space>
                <Avatar className='avatar'  icon={<UserOutlined />} />
              </Space>
            </a>
          </Dropdown>

        </Header>


        <Content
          className="site-layout-content"
          style={{
            margin: '5px',
            padding: '10px',
            minHeight: 280,
          }}
        >
          {routerComps}
        </Content>
      </Layout>
    </Layout>
  );
};