import { useState, useEffect } from "react";
import { Modal, Form, Select, Input, DatePicker, Radio } from "antd";
import API from "../../api/index";
import { famterDate } from "../../utils/famterDate";
import dayjs from "dayjs";

const { RangePicker } = DatePicker;

export default function AddModal(props) {
  const [insertForm] = Form.useForm();
  const dateFormat = "YYYY/MM/DD";


  const [areas, setAreas] = useState([]);

  useEffect(() => {
    getArea();
  }, []);


  const getArea = async () => {
    const data = await API.getArea();
    setAreas(data);
  };


  //下拉框
  const businessComponet = () => {
    const options = [
      { label: "大型级以上餐饮", value: "1" },
      { label: "中型餐饮", value: "2" },
      { label: "小微型餐饮", value: "3" },
      { label: "学校食堂", value: "4" },
      { label: "托幼机构食堂", value: "5" },
      { label: "养老机构食堂", value: "6" },
      { label: "医疗机构食堂", value: "7" },
      { label: "其他", value: "8" },
    ];

    return (<Select style={{ width: 120, }} options={options} />);

  };

  //下拉框
  const areaComponet = () => {

    const options = areas.map((item) => { return { label: item.name, value: item.id, }; });

    return (<Select style={{ width: 120, }} options={options} />);
  };


  const handleOk = async () => {
    const data = insertForm.getFieldsValue(true);
    if (data.validTime) {
      data.validStartTime = famterDate(new Date(data.validTime[0]));
      data.validEndTime = famterDate(new Date(data.validTime[1]));
    }

    if (props.editId) {
      await API.updateCompany(data);
    } else {
      await API.addCompany(data);
    }
    
    props.onModalClose();
    insertForm.resetFields();
    props.setEditId(null);
  };

  const handleCancel = () => {
    props.onModalClose();
  };

  useEffect(() => {
    if (props.visible && props.editId) {
      openEdit(props.editId);
    }
  }, [props.visible]);



  const openEdit = async (id) => {
    const data = await API.getCompanyInfo(id);

    if (data.areaId) {
      data.areaId = Number(data.areaId);
    }

    if (data.validEndTime) {
      data.validTime = [dayjs(data.validStartTime, dateFormat), dayjs(data.validEndTime, dateFormat),];
    }

    insertForm.setFieldsValue(data);
  };

  return (
    <Modal
      title={props.editId ? "编辑" : "新增"}
      open={props.visible}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <Form form={insertForm}>
        <Form.Item label="所属辖区" name="areaId">
          {areaComponet()}
        </Form.Item>
        <Form.Item label="经营类型" name="businessType">
          {businessComponet()}
        </Form.Item>
        <Form.Item label="单位名称" name="companyName">
          <Input></Input>
        </Form.Item>
        <Form.Item label="有效日期" name="validTime">
          <RangePicker />
        </Form.Item>
        <Form.Item label="营业状态" name="businessStatus">
          <Radio.Group>
            <Radio value="on">正常营业</Radio>
            <Radio value="off">暂停营业</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  );
}
