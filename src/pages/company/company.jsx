import {
  Space, Button, Form, Select, Input, Table, DatePicker, Modal, Radio
} from "antd";
import { useEffect, useState } from "react";
import API from "../../api/index"
import { famterDate } from "../../utils/famterDate";
const { confirm } = Modal;
import './company.scss'
import dayjs from "dayjs";
const { RangePicker } = DatePicker;
import AddModal from "./AddModal";

export default function Company() {

  const [total, setTotal] = useState(0);

  const [searchForm] = Form.useForm();

  const [isLoading, setIsLoading] = useState(false);

  const [insertForm] = Form.useForm();

  const dateFormat = "YYYY/MM/DD";

  const [areas, setAreas] = useState([]);

  const [visible, setVisible] = useState(false);


  const handleModalClose = () => {
    setVisible(false);
    setFilterParams({
      ...filterParams,
      page:1,
    });

  };

  const handleEdit = (id) => {
    setEditId(id);
    setVisible(true);
  };

  const businessTypes = [
    { label: "全部", value: "" },
    { label: "大型级以上餐饮", value: "1" },
    { label: "中型餐饮", value: "2" },
    { label: "小微型餐饮", value: "3" },
    { label: "学校食堂", value: "4" },
    { label: "托幼机构食堂", value: "5" },
    { label: "养老机构食堂", value: "6" },
    { label: "医疗机构食堂", value: "7" },
    { label: "其他", value: "8" },
  ];

  const columns = [
    {
      title: '序号',
      dataIndex: 'id',
      align: 'center',
      key: 'id',
      render: (textext, record, index) => <a>{index + 1}</a>,
    },
    {
      title: "所属辖区",
      align: 'center',
      dataIndex: "areaName",
      key: "id",
    },
    {
      title: "经营类型",
      dataIndex: "businessType",
      align: 'center',
      key: "id",
      render: (text) => {
        return (
          <div>{businessTypes.find((item) => item.value === text)?.label}</div>

        );
      },
    },
    {
      title: "单位名称",
      dataIndex: "companyName",
      align: 'center',
      key: "id",
    },
    {
      title: "许可证有效期",
      dataIndex: "id",
      align: 'center',
      key: "id",
      render: (text, record) => (
        <div>
          {famterDate(record.validStartTime)}-{famterDate(record.validEndTime)}
        </div>
      ),
    },
    {
      title: "营业状态",
      dataIndex: "businessStatus",
      align: 'center',
      key: "id",
      render: (text) => {
        if (text) {
          return <div>{text === "on" ? "营业中" : "暂停营业"}</div>;
        }
        return "";
      },
    },
    {
      title: "创建时间",
      dataIndex: "id",
      align: 'center',
      key: "id",
      render: (text, record) => (
        <div>
          {famterDate(record.createTime)}
        </div>
      )
    },
    {
      title: "操作",
      dataIndex: "id",
      align: 'center',
      render: (text, record) => (
        <div>
          <Button onClick={() => handleEdit(record.id)}>编辑</Button>
          <Button onClick={() => del(record.id)}>删除</Button>
        </div>
      )
    },
  ];

  const del = (id) => {
    confirm({
      title: "确定要删除吗？",
      async onOk() {
        // 拿到要删除的id

        // 单删还是多删
        const ids = id ? [id] : delIds;

        // 调用删除接口
        await API.delCompany(ids);

        // 重新调用接口，其实就是重置分页参数
        setFilterParams({
          ...filterParams,
          page: 1,
        });

        // 清空ids
        setDelIds([]);
      },
    });
  };




  const [data, setData] = useState([])

  const [delIds, setDelIds] = useState([]);

  const [editId, setEditId] = useState(null);

  const [filterParams, setFilterParams] = useState({
    page: 1,
    size: 3,
    sort: "desc",
    order: "createTime",
  });



  useEffect(() => {
    getCompany();
  }, [filterParams])


  const getCompany = async (data) => {
    setIsLoading(true);
    const res = await API.getCompany(filterParams)
    res.list.forEach((item) => {
      item.key = item.id
    })
    setData(res.list)
    setTotal(res.pagination.total);
    setIsLoading(false);
  }


  const paginationChange = (page, size) => {
    // 修改数据
    setFilterParams({
      ...filterParams,
      page,
    });
  };




  useEffect(() => {
    getArea();
  }, []);

  // 所属辖区
  const getArea = async () => {
    const data = await API.getArea();
    setAreas(data);
  };


  const businessComponet = (isFilter) => {
    const originOptions = [
      { label: "大型级以上餐饮", value: "1" },
      { label: "中型餐饮", value: "2" },
      { label: "小微型餐饮", value: "3" },
      { label: "学校食堂", value: "4" },
      { label: "托幼机构食堂", value: "5" },
      { label: "养老机构食堂", value: "6" },
      { label: "医疗机构食堂", value: "7" },
      { label: "其他", value: "8" },
    ];

    const options = isFilter
      ? [{ label: "全部", value: "" }, ...originOptions]
      : originOptions;
    return (
      <Select
        style={{
          width: 120,
        }}
        options={options}
      />
    );
  };

  const licenceStatus = [
    {
      label: "正常",
      value: "NORMAL",
    },
    {
      label: "过期",
      value: "BEOVERDUE",
    },
    {
      label: "即将过期",
      value: "NEAREXPIRATION",
    },
  ];


  const reset = () => {
    // 重置表单,只是重置了组件的表现
    searchForm.resetFields();
    // 重置过滤数据
    setFilterParams({
      page: 1,
      size: 3,
      sort: "desc",
      order: "createTime",
    });
  };

  const searchData = (data) => {
    console.log("搜索内容:", data);
    setFilterParams({
      ...filterParams,
      ...data,
      page: 1,
    });
  };

  const rowSelectionChagne = (vals, rows) => {
    console.log("选择了:", vals, rows);
    setDelIds(vals);
  };


  const openEdit = async (id) => {
    console.log('openEdit');
    const data = await API.getCompanyInfo(id);

    if (data.areaId) {
      data.areaId = Number(data.areaId);
    }

    if (data.validEndTime) {
      data.validTime = [
        dayjs(data.validStartTime, dateFormat),
        dayjs(data.validEndTime, dateFormat),
      ];
    }
    setIsModalOpen(true);
    setEditId(id);
    insertForm.setFieldsValue(data);

  };



  return (
    <div className="company" >
      <Space className="filter" wrap={false}>

        <Space>
          <Button onClick={getCompany}>刷新</Button>
          <Button type="primary" onClick={() =>  setVisible(true)}>新增</Button>
          <Button danger onClick={() => del()} disabled={!delIds.length}>删除</Button>
        </Space>

        <Form layout="inline" form={searchForm} onFinish={searchData}>
          <Form.Item name="areaId" label="所属辖区">
            <Select
              placeholder="请选择"
              style={{
                width: 120,
              }}
              options={[
                { value: "", label: "全部", },
                ...areas.map((item) => {
                  return {
                    label: item.name,
                    value: item.id
                  }
                })
              ]}
            />
          </Form.Item>
          <Form.Item name="businessType" label="经营状态">
            {businessComponet("filter")}
          </Form.Item>
          <Form.Item label="许可状态">
            <Select
              style={{
                width: 120,
              }}
              options={[{ label: "全部", value: "" }, ...licenceStatus]}
            />
          </Form.Item>
          <Form.Item name="keyWord">
            <Input placeholder="请输入"></Input>
          </Form.Item>

          <Space>
            <Button type="primary" htmlType="submit">搜索</Button>
            <Button onClick={() => reset()}>重置</Button>
          </Space>
        </Form>
      </Space>


      <Table columns={columns} dataSource={data}
        bordered
        pagination={{
          current: filterParams.page,
          pageSize: filterParams.size,
          showQuickJumper: true,
          total,
          onChange: paginationChange,

        }}
        loading={isLoading}
        rowSelection={{
          type: "checkbox",
          // 每次选择框变化的数据，就是我们设置的key
          onChange: rowSelectionChagne,
        }}
      />

      <AddModal
        visible={visible}
        onModalClose={handleModalClose}
        editId={editId}
        setEditId={setEditId}
      />
    </div>
  );
}
