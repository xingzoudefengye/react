import FilterComponent from "../../components/filter-component";
import { Form, Input, Table, Select, DatePicker, Space, Button } from "antd";
import useFetchList from "./../../hooks/use-fetch-list";
import useApi from "./../../api/useApi";
import { useState } from "react";
import { famterDate } from "../../utils/famterDate";

const { RangePicker } = DatePicker;

export default function Supervise() {
  const [searchForm] = Form.useForm();

  const [delIds, setDelIds] = useState([]);

  const inspectApi = useApi("/admin/inspectionSupervision/inspec");

  const { dataSource, total, filterParams, getData, setFilterParams } =
    useFetchList({
      API: inspectApi.getList,
      delIds,
    });

  const inspectResults = [
    {
      label: "合格",
      value: "A",
    },
    {
      label: "不合格",
      value: "B",
    },
    {
      label: "已整改",
      value: "C",
    },
  ];

  const columns = [
    {
      title: "序号",
      // dataSource数组里面对应要展示的字段
      dataIndex: "id",
      align: 'center',
      key: "id",
      // render这个方法重写表格里的内容
      render: (text, record, index) => <a>{index + 1}</a>,
    },
    {
      title: "检查时间",
      align: 'center',
      dataIndex: "inspectTime",
      key: "id",
    },
    {
      title: "检查结果",
      align: 'center',
      dataIndex: "inspectResult",
      key: "id",
    },
    {
      title: "单位名称",
      align: 'center',
      dataIndex: "company",
      key: "id",
    },
    {
      title: "许可状态",
      align: 'center',
      dataIndex: "licenceStatus",
      key: "id",
    },
    {
      title: "创建时间",
      dataIndex: "id",
      align: 'center',
      key: "id",
      render: (text, record) => (
        <div>
          {famterDate(record.createTime)}
        </div>
      )
    },
    {
      title: "操作",
      align: 'center',
      dataIndex: "id",
      render: () => (
        <Space>
          <Button>删除</Button>
          <Button>编辑</Button>
        </Space>
      ),
    },
  ];

  const add = () => {};

  const del = () => {};

  const searchData = () => {
    const data = searchForm.getFieldsValue(true);
    if (data.inspectTime) {
      data.inspectStartTime = new Date(data.inspectTime[0]);
      data.inspectEndTime = new Date(data.inspectTime[1]);
    }
    setFilterParams({ ...filterParams, ...data });
  };

  const reset = () => {
    searchForm.resetFields();
    setFilterParams({
      page: 1,
      size: 3,
      sort: "desc",
      order: "createTime",
    });
  };

  return (
    <div>
      <FilterComponent
        del={del}
        reload={getData}
        add={add}
        delIds={[]}
        searchForm={searchForm}
        searchData={searchData}
        reset={reset}
      >
        <Form.Item label="检查结果" name="inspectResult">
          <Select
            style={{
              width: 120,
            }}
            options={inspectResults}
          />
        </Form.Item>
        <Form.Item label="检查时间" name="inspectTime">
          <RangePicker />
        </Form.Item>
      </FilterComponent>
      <Table
        style={{ marginTop: "20px" }}
        columns={columns}
        dataSource={dataSource}
        pagination={{
          current: filterParams.page,
          pageSize: filterParams.size,
          total,
          onChange: (page) => {
            setFilterParams({ ...filterParams, page });
          },
        }}
        rowSelection={{
          type: "checkbox",
          // 每次选择框变化的数据，这个数据就是我们设置的key
          onChange: (ids) => setDelIds(ids),
        }}
      />
    </div>
  );
}
