import "./login.scss"
import { Card, Tabs, Button, Checkbox, Form, Input } from 'antd';
import React, { useState } from 'react';
import API from '../../api/auth'
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUserInfo } from "./../../store/slices/user";



/* http://localhost:5173/#/login */
export default function Login() {

  const [role, setRole] = useState("inspec");

  const navigate = useNavigate()

  const dispath = useDispatch();


  const items = [
    {
      key: "inspec",
      label: `监督检查管理系统`,
    },
    {
      key: "familyPartyRecord",
      label: `农村家宴管理系统`,
    },
  ]

  const login = async (values) => {
    // 调用登录接口
    const data = await API.login({ ...values, role })
    // 把token存到localstorage里面
    localStorage.setItem("token", data.token);
    // 拿到当前账号的权限
    const userInfo = await API.getUserInfo();
    dispath(
      setUserInfo({
        roles: userInfo.roles,
      })
    );
    // 保存角色渲染对应的菜单
    localStorage.setItem("roles", JSON.stringify(userInfo.roles));
    // 跳转到首页(根据角色展示菜单)
    // 跳转到对应页面
    navigate(
      userInfo.roles.includes("inspec") ? "/storeManage/company" : "/storeManage/culturalAuditorium"
    );
  };

  const changeRole = (key) => {
    console.log(key);
    setRole(key)
  }

  return (
    <div className="login">
      (
      <Card
        bordered={false}
        style={{
          width: 400,
          textAlign: "center"
        }}
      >
        <h2>请登录</h2>
        <Tabs
          defaultActiveKey="inspec"
          onChange={(key) => changeRole(key)}
          centered
          items={items}
        />
        <Form onFinish={login}>
          <Form.Item
            label="用户名"
            name="username"
            rules={[
              {
                required: true,
                message: "请输入用户名！",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="密码"
            name="password"
            rules={[
              {
                required: true,
                message: "请输入密码！",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 4,
              span: 16,
            }}
          >

            <Button type="primary" htmlType="submit">
              登录
            </Button>
          </Form.Item>
        </Form>
      </Card>
      )
    </div>
  )
}