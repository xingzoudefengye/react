import { Space, Button, Form, Input } from "antd";

/**
 *
 * @param {*} param0
 * @returns
 */
export default function FilterComponent({
  /** 刷新 */
  reload,
  /** 添加 */
  add,
  /** 删除的id */
  delIds,
  /** 删除的方法 */
  del,
  /** useForm返回的 */
  searchForm,
  /** 搜索 */
  searchData,
  /** 重置 */
  reset,
  children,
}) {
  return (
    <Space>
      <Button onClick={() => reload()}>刷新</Button>
      <Button type="primary" onClick={() => add()}>
        新增
      </Button>
      <Button disabled={!delIds.length} danger onClick={() => del()}>
        删除
      </Button>
      <Form form={searchForm} layout="inline" onFinish={searchData}>
        {children}
        <Form.Item name="keyWord">
          <Input></Input>
        </Form.Item>
        <Space>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
          <Button onClick={() => reset()}>重置</Button>
        </Space>
      </Form>
    </Space>
  );
}
