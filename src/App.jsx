import { useState } from 'react'
import './App.css'
import { HashRouter, Route, Routes } from 'react-router-dom'
import Home from './pages/home/home'
import Login from "./pages/login/login";
import { getMenus } from './pages/home/menu'
/* 根组件 */
function App() {

  const routes = getMenus();
  return (
    <HashRouter>
      <Routes>
        <Route path="/login" element={<Login></Login>}></Route>
        <Route path="/*" element={<Home></Home>}></Route>
      </Routes>
    </HashRouter>
  )
}

export default App
